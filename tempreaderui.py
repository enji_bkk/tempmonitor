#!/usr/local/bin/python3

import configreader
import temperaturereader
import datetime
import argparse
import time

# For async 
import threading
import queue

# For UI
from tkinter import *
from tkinter import ttk


def getArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument("-c", "--config-file", help="The config file", required=True)
  return parser.parse_args()



class Gui:

  def __init__(self, master, queue, period):

    self.queue=queue
    self.master=master
    self.period=period

    mainframe = ttk.Frame(master, padding="3 3 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))

    # The 'view model'
    self.cpu0 = StringVar()
    self.cpu1 = StringVar()
    self.gpu = StringVar()

    # Static text labels
    ttk.Label(mainframe, text="cpu0:").grid(column=0, row=0, sticky=(W, E))
    ttk.Label(mainframe, text="cpu1:").grid(column=0, row=1, sticky=(W, E))
    ttk.Label(mainframe, text="gpu:").grid(column=0, row=2, sticky=(W, E))

    # Dynamic text labels
    ttk.Label(mainframe, textvariable=self.cpu0).grid(column=1, row=0, sticky=(W, E))
    ttk.Label(mainframe, textvariable=self.cpu1).grid(column=1, row=1, sticky=(W, E))
    ttk.Label(mainframe, textvariable=self.gpu).grid(column=1, row=2, sticky=(W, E))

  # This function will be periodically called by the UI framework (using the after() method)
  # to read the queue and update the display
  # N.B.: this is run on the main thread => safe to update 'TK stuff' (widgets or bound variables)
  def pollQueueAndUpdate(self):
    while self.queue.qsize():
      try:
        msg = self.queue.get(0)
        # Check contents of message and do what it says
        # As a test, we simply print it
        # TODO : use logging
        print(msg)
        self.cpu0.set(msg[0])
        self.cpu1.set(msg[1])
        self.gpu.set(msg[2])
      except Queue.Empty:
        pass

  # Starts the loop to poll queue and update UI
  def periodicPoll(self):
    self.pollQueueAndUpdate()
    self.master.after(self.period*333, self.periodicPoll) # 333 = 1000/3

class App:
  
  def __init__(self, master, tempreader, confreader):
    self.queue=queue.Queue(10)

    self.master=master
    self.gui=Gui(master, self.queue, confreader.getPeriod())

    self.tempreader=tempreader
    self.confreader=confreader


    # Start the loop to
    # - read temps from system calls
    # - push read value into queue
    # in a separate thread
    self.thread1=threading.Thread(target=self.periodicReadAndQueue)
    self.thread1.daemon=True
    self.thread1.start()

    # Start the UI queue polling function
    self.gui.periodicPoll()

  # This function calls f every period, passing *args.
  def do_every(self, period, f, *args):
      def g_tick():
          t = time.time()
          count = 0
          while True:
              count += 1
              yield max(t + count*period - time.time(),0)
      g = g_tick()
      while True:
          time.sleep(next(g))
          f(*args)  

  # Read from sys calls and push result to UI communication queue
  def readAndQueue(self):
    temps=[ x.decode() for x in self.tempreader.read() ]
    # TODO: use logger
    print(temps)

    self.queue.put(temps)

  # TODO: try to replace with a lambda
  def periodicReadAndQueue(self):
    self.do_every(self.confreader.getPeriod(), self.readAndQueue)

def main():
  args=getArgs()
  print(args)
  confReader=configreader.configReader(args.config_file)
  tempReader=temperaturereader.TemperatureReader(confReader)

  #logFile=confReader.getOutputFile()

  root = Tk()  # main window
  theApp=App(root, tempReader, confReader)

  root.mainloop()  

if __name__ == "__main__":
  main()
