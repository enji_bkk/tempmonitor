import subprocess
import re
import traceback
import sys

class TemperatureReader:

  def __init__(self, iConfigReader):
    self.config_reader = iConfigReader

  def getCPUTemps(self):
    result = []
    for aCpuLabel in self.config_reader.getCPULabels():
      try:
        res=subprocess.check_output( ['sysctl', '-n', aCpuLabel] )
        print(res)
        match=re.search(rb'[0-9]+\.[0-9]+', res)
        result.append(match.group())
      except:
        print("System call failed")
        traceback.print_exc(file=sys.stdout)

    return result

  def getGPUTemp(self):
    result=None
    try:
      result=subprocess.check_output( ['/usr/local/bin/nvidia-settings', '-c', self.config_reader.getXDisplay(), '-q', 'gpucoretemp', '-t'] ).rstrip()+b'.0'
      print(result)
    except:
      print("System call failed 2")
      traceback.print_exc(file=sys.stdout)

    return result

  # Return all temps as a list
  def read(self):
    tmp=self.getCPUTemps()
    tmp.append(self.getGPUTemp())
    return tmp


