import json

class configReader:
  
  def __init__( self, iFileName ):
    self.filename = iFileName
    self.parseFile()

  def parseFile( self ):
    with open(self.filename, 'r') as f:
      self.json = json.load(f)

  def getXDisplay( self ):
    return self.json["nvidia"]["display"]

  def getCPULabels( self ):
    return self.json["cpu"]["labels"]

  def getOutputFile( self ):
    return self.json["options"]["output_file"]

  # Return the periodicity at which temps are checked
  # If undefined in the conf file: default to 10
  def getPeriod( self ):
    if "period"in self.json["options"]:
      return self.json["options"]["period"]
    else:
      return 10
