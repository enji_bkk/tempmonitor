

# Easy implementation with loop on each element in the arrays.

def encobs( iBytes ):
  # Sanity check : encoding of stretches of more than 254 non-0 characters is not supported
  # at this stage so to be safe...
  if len( iBytes ) > 254:
    raise Exception( "Does not support size above 254" )

  # Result : store as bytearray
  result = bytearray( len( iBytes ) + 1 )

  # Position of the last found \x00 character
  lastZeroByte = 0

  # Loop on iBytes
  for aIndex in range(len(iBytes)):
    # Always write the byte
    result[ aIndex + 1 ] = iBytes[ aIndex ]

    # Found a \x00 :
    # - compute the length of the last stretch of non-0 bytes (including its precedeing \x00)
    # - store it in place of the last \x00 we found
    if iBytes[ aIndex ] == 0:
      result[ lastZeroByte ] = aIndex + 1 - lastZeroByte
      lastZeroByte = aIndex + 1

  # Finalize the last field
  result[ lastZeroByte ] = len( iBytes ) + 1 - lastZeroByte
  result.append(0)

  return result


def decobs( iBytes ):
  result = bytearray( len( iBytes ) - 2 )

  currentNonZeroStretch = iBytes[ 0 ]
  
  # if result is 0 : the 1st element of the original string was a 

  for aIndex in range( 1, len( iBytes ) - 1 ):
    currentNonZeroStretch = currentNonZeroStretch - 1

    # End of this stretch of non 0 bytes found
    if currentNonZeroStretch == 0:
      # Read the length of next non-0 bytes
      currentNonZeroStretch = iBytes[ aIndex ]

      # We have reached the position of a \x00 in the original message : write \x00
      result[ aIndex - 1 ] = 0

    else:
      result[ aIndex - 1 ] = iBytes[ aIndex ]

  return result
