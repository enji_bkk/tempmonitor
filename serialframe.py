#!/usr/local/bin/python3

import serial
import argparse
import time
import cobs

def getArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", "--input-string", help="String to send through serial", required=True)
  parser.add_argument("-s", "--serial-port", help="Port to send to", required=True)
  return parser.parse_args()

START_END=0x7E
ESCAPE=0x7D
ESCAPE_XOR=0x20

class FrameSender:
  def __init__(self):
    self.frame_max_length = 8
    self.in_escape = False

  def getFrames(self, iData):
    aFrame = bytearray()
    aFrame.append( START_END )
    for aByte in iData:
      if iData in [ START_END, ESCAPE ]:
        # Need to escape this...
        aByte ^= ESCAPE_XOR
        aFrame.extend( [ ESCAPE, aByte ] )

      else:
        aFrame.append( aByte )

    aFrame.append( START_END )

    return aFrame

if __name__ == "__main__":

  args=getArgs()

  print(args)

  # Open port with default param: 9600, 8, N, 1
  com = serial.Serial()
  com.port = args.serial_port 
  com.baudrate = 9600
  com.timeout = 1
  com.setDTR(True)
  com.open()

  time.sleep(2)
  print(args.input_string.encode())
  frame = cobs.encobs(args.input_string.encode()) 
  print(frame)
  com.write(frame)


  com.close()

  #with serial.Serial() as ser:
    #ser.baudrate = 9600
    #ser.port = args.serial_port
    #ser.setDTR(False)
    #ser.open()
    # Send the string
    #ser.write(args.input_string.encode())
    #time.sleep(10)
   

