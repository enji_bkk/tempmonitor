#!/usr/local/bin/python3

import configreader
import temperaturereader
import datetime
import argparse
import time

# TODO : use logging framework
def formatForLog(iTempReader):
  timestamp=datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
  cpu0, cpu1 = [x.decode() for x in iTempReader.getCPUTemps()]
  gpu = iTempReader.getGPUTemp().decode() if iTempReader.getGPUTemp() is not None else None
  return "{0} - cpu0: {1} - cpu1: {2} - gpu: {3}".format(timestamp, cpu0, cpu1, gpu)

def getArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument("-c", "--config-file", help="The config file", required=True)
  return parser.parse_args()

# Auxiliary function to print the formatted log.
def doPrint(iTempReader):
  print(formatForLog(iTempReader))

# Auxiliary function to wrtie to file
def doWrite(iFile, iTempReader):
  iFile.write(formatForLog(iTempReader)+'\n')
  iFile.flush()

#This function calls f every period, passing *args.
def do_every(period,f,*args):
    def g_tick():
        t = time.time()
        count = 0
        while True:
            count += 1
            yield max(t + count*period - time.time(),0)
    g = g_tick()
    while True:
        time.sleep(next(g))
        f(*args)  

def main():
  args=getArgs()
  print(args)
  confReader=configreader.configReader(args.config_file)
  tempReader=temperaturereader.TemperatureReader(confReader)

  logFile=confReader.getOutputFile()

  with open( logFile, 'a' ) as f:
    do_every(10, doWrite, f, tempReader)


if __name__ == "__main__":
  main()
