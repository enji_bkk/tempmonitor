import os
import re
import sys
import unittest
from unittest import mock

# To find the module we want to test
sys.path.insert(0, os.path.abspath('..'))
# Testing configreader module
import temperaturereader


class TestConfigReader(unittest.TestCase):

  def setUp(self):
    # Noob's test : uncomment to see that setUp() is called before running each function
    # (and not once at the begining)
    #print("enter setUp()")

    # Create a mock for the config reader.
    # Allows us to customize the return values seen by temperaturereader
    self.confReader=mock.Mock()
    self.confReader.getCPULabels.return_value=['dev.cpu.0.temperature']
    self.confReader.getXDisplay.return_value=":0.0"

    self.tempReader=temperaturereader.TemperatureReader(self.confReader)

  @mock.patch('temperaturereader.subprocess')
  def test_getCPUTemps_1core(self, subprocess_mock):
    # define the behaviour of the mock: always return 16.2C
    subprocess_mock.check_output=mock.Mock(return_value=b'16.2C\n') 

    # check the result
    self.assertEqual(self.tempReader.getCPUTemps(), [b'16.2'])

  @mock.patch('temperaturereader.subprocess')
  def test_getCPUTemps_2cores(self, subprocess_mock):
    # define behaviour of the mocks
    # subprocess_mock returns a different value during the second call
    subprocess_mock.check_output=mock.Mock(side_effect=[b'16.2C\n', b'17.3C\n'])

    # Will read temperatures from 2 cpu cores
    self.confReader.getCPULabels.return_value=['dev.cpu.0.temperature', 'dev.cpu.1.temperature']

    # check the result
    self.assertEqual(self.tempReader.getCPUTemps(), [b'16.2', b'17.3'])

  @mock.patch('temperaturereader.subprocess')
  def test_getCPUTemps_subprocess_exceptioni(self, subprocess_mock):
    subprocess_mock.check_output=mock.Mock(side_effect=Exception('Boom!'))

    self.assertEqual(self.tempReader.getCPUTemps(),[])



  # Not really a unit test : performs a real system call to read temparature
  # Keep it simple : only 1 core.
  # As temperature will change, use a regexp
  def test_getCPUTemps_1core_real_call(self):

    res=self.tempReader.getCPUTemps()
    #print(res)
    self.assertEqual(len(res), 1)
    self.assertTrue(re.match(rb'^[0-9]+\.[0-9]$', res[0]))

  @mock.patch('temperaturereader.subprocess')
  def test_getGPUTemp(self, subprocess_mock):
    # define the simulated return value for reading the temperature
    subprocess_mock.check_output=mock.Mock(return_value=b'32')

    self.assertEqual(self.tempReader.getGPUTemp(), b'32.0')

  @mock.patch('temperaturereader.subprocess')
  def test_getGPUTemp_subprocess_exception(self, subprocess_mock):
    subprocess_mock.check_output=mock.Mock(side_effect=Exception('Boom!'))

    self.assertIsNone(self.tempReader.getGPUTemp())

  #Not really a unit test as the system call is actually made.
  def test_getGPUTemp_real_call(self):
    res=self.tempReader.getGPUTemp()
    self.assertTrue(re.match(rb'[0-9]+\.0', res))


  @mock.patch('temperaturereader.subprocess')
  def test_read(self, subprocess_mock):
    subprocess_mock.check_output=mock.Mock(side_effect=[b'16.2C\n', b'17.3C\n', b'33'])

    # Will read temperatures from 2 cpu cores
    self.confReader.getCPULabels.return_value=['dev.cpu.0.temperature', 'dev.cpu.1.temperature']
    
    res=self.tempReader.read()
    print(res)
    self.assertEqual(res, [b'16.2', b'17.3', b'33.0'])

if __name__ == '__main__':
    unittest.main()

