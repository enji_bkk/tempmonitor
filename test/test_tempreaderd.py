import os
import re
import sys
import unittest
from unittest import mock

# To find the module we want to test
sys.path.insert(0, os.path.abspath('..'))
# Testing configreader module
import tempreaderd


class TestConfigReader(unittest.TestCase):

  def setUp(self):
    # Create a mock for the config reader.
    # Allows us to customize the return values seen by temperaturereader
    self.tempReader=mock.Mock()
    self.tempReader.getCPUTemps.return_value=[b'12.2C', b'13.3C']
    self.tempReader.getGPUTemp.return_value=b'14.0C'


  def test_formatForLog(self):
    # check the result
    result=tempreaderd.formatForLog(self.tempReader)
    self.assertTrue(re.match(r'[0-9]{8}_[0-9]{6} - cpu0: 12.2C - cpu1: 13.3C - gpu: 14.0C', result))

if __name__ == '__main__':
    unittest.main()

