import os
import sys
import unittest

# To find the module we want to test
sys.path.insert(0, os.path.abspath('..'))
# Testing configreader module
import cobs


class TestCobsEncode(unittest.TestCase):

  def setUp(self):
    pass

  def test_encodeNonZeros(self):
    expectedValue = bytearray(b'\x05Toto\x00')
    self.assertEqual(cobs.encobs(b'Toto'), expectedValue)

  def test_encodeZeroAtMiddle(self):
    expectedValue = bytearray(b'\x05Toto\x05Tutu\x00')
    self.assertEqual(cobs.encobs(b'Toto\x00Tutu'), expectedValue)

  def test_encodeZeroAtStart(self):
    expectedValue = bytearray(b'\x01\x05Toto\x00')
    self.assertEqual(cobs.encobs(b'\x00Toto'), expectedValue)

  def test_encodeZeroAtEnd(self):
    expectedValue = bytearray(b'\x05Toto\x01\x00')
    self.assertEqual(cobs.encobs(b'Toto\x00'), expectedValue)


class TestCobsDecode(unittest.TestCase):

  def setUp(self):
    pass

  def test_decodeNonZeros(self):
    expectedValue = bytearray(b'Toto')
    self.assertEqual(cobs.decobs(b'\x05Toto\x00'), expectedValue)

  def test_decodeZeroAtMiddle(self):
    expectedValue = bytearray(b'Toto\x00Tutu')
    self.assertEqual(cobs.decobs(b'\x05Toto\x05Tutu\x00'), expectedValue)

  def test_decodeZeroAtStart(self):
    expectedValue = bytearray(b'\x00Toto')
    self.assertEqual(cobs.decobs(b'\x01\x05Toto\x00'), expectedValue)

  def test_decodeZeroAtEnd(self):
    expectedValue = bytearray(b'Toto\x00')
    self.assertEqual(cobs.decobs(b'\x05Toto\x01\x00'), expectedValue)


if __name__ == '__main__':
    unittest.main()
