import os
import sys
import unittest

# To find the module we want to test
sys.path.insert(0, os.path.abspath('..'))
# Testing configreader module
import configreader


class TestConfigReader(unittest.TestCase):

  def setUp(self):
    self.confReader=configreader.configReader("dummy.conf")

  def test_getXDisplay(self):
    self.assertEqual(self.confReader.getXDisplay(), 'localhost:0.0')

  def test_getCPULabels(self):
    self.assertEqual(self.confReader.getCPULabels(), ['dev.cpu.0.temperature', 'dev.cpu.1.temperature', 'dev.cpu.3.temperature'])
  
  def test_getOutputFile(self):
    self.assertEqual(self.confReader.getOutputFile(), '/var/log/temps.log')  

  def test_getPeriod(self):
    self.assertEqual(self.confReader.getPeriod(), 5)
  
  def test_getPeriodDefault(self):
    # manually remove the key in the json structure
    del self.confReader.json["options"]["period"]
    self.assertEqual(self.confReader.getPeriod(), 10)

if __name__ == '__main__':
    unittest.main()
