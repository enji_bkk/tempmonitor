import os
import sys
import unittest

# To find the module we want to test
sys.path.insert(0, os.path.abspath('..'))
# Testing configreader module
import serialframe


class TestSerialFrame(unittest.TestCase):

  def setUp(self):
    self.frameSender = serialframe.FrameSender()

  def test_buildFrame1(self):
    expectedValue = bytearray()
    expectedValue.append(serialframe.START_END)
    expectedValue.extend(b'Toto')
    expectedValue.append(serialframe.START_END)
    print( expectedValue )
    self.assertEqual(self.frameSender.getFrames(b'Toto'), expectedValue)

  def test_buildFrame1(self):
    expectedValue = bytearray()
    expectedValue.append(serialframe.START_END)
    expectedValue.extend(b'Toto')
    expectedValue.append(serialframe.START_END)
    print( expectedValue )
    self.assertEqual(self.frameSender.getFrames(b'Toto'), expectedValue)



if __name__ == '__main__':
    unittest.main()
