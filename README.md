# Temperature Monitor #

## Description
This script reads temperature from CPU and GPU on old laptop running freeBSD. Results are displayed in a small UI.

The goal is to send the readings through serial interface to arduino board (using cobs protocol)

To read the temperature sensors, it relies a heavily on the system tools and utilities (commands run trhough a shell)

* freeBSD's system tool to read AMD CPU core temperatures.
* nvidia-settings to read GPU tempretaure

## Why bother ?
Off the shelf products exist but under freebsd I did not manage to make them work on my old laptop.
Also wanted something to export data to arduino board, ...

# Dependencies
* nvidia-settings (to access GPU temps). N.B.: an X server must be running for it to operate
* freeBSD system tools for AMD CPUs (need to find the name)
* python 3.4
* nosetests (to run tests)
* Deployment instructions

# Status
* temperature monitoring + UI display works
* arduino communication : in progress